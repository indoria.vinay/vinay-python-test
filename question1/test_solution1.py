from question1.solution import create_dictionary

# checking for only strings
def test_create_dictionary_text():
    key_list = ['Hello','Bonjour','Hola']
    val_list = ['Vinay','David','Patric']
    key_val_dict = create_dictionary(key_list, val_list)
    sample_key_val_dict = {'Hello':'Vinay', 'Bonjour':'David', 'Hola':'Patric'}
    assert sample_key_val_dict == key_val_dict


# checking for only numbers
def test_create_dictionary_number():
    key_list = [1,2,3]
    val_list = [4,5,6]
    key_val_dict = create_dictionary(key_list, val_list)
    sample_key_val_dict = {1:4, 2:5, 3:6}
    assert sample_key_val_dict == key_val_dict

# checking for both number and text
def test_create_dictionary_text_number():
    key_list = [1,2,3]
    val_list = ['Vinay','David','Patric']
    key_val_dict = create_dictionary(key_list, val_list)
    sample_key_val_dict = {1:'Vinay', 2:'David', 3:'Patric'}
    assert sample_key_val_dict == key_val_dict

# value list elements is greater than key list 
def test_create_dictionary_val_greater():
    key_list = [1,2,3]
    val_list = [4,5,6,7,8]
    key_val_dict = create_dictionary(key_list, val_list)
    sample_key_val_dict = {1:4, 2:5, 3:6}
    assert sample_key_val_dict == key_val_dict

# key list elements are greater than value list
def test_create_dictionary_key_greater():
    key_list = [1,2,3,9]
    val_list = [4,5,6]
    key_val_dict = create_dictionary(key_list, val_list)
    sample_key_val_dict = {1:4, 2:5, 3:6,9:None}
    assert sample_key_val_dict == key_val_dict

# passing blank value for both the lists
def test_create_dictionary_empty():
    key_list = list()
    val_list = list()
    key_val_dict = create_dictionary(key_list, val_list)
    sample_key_val_dict = {}
    assert sample_key_val_dict == key_val_dict
