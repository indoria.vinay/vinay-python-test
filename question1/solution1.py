import sys

# the function to form a dictionary based on given key_list and val_list
def create_dictionary(key_list, val_list):
    # the zip function is very close to our requirement but it considers the least possible length
    # in all the given lists
    val_list_length = len(val_list)
    key_val_dictionary = dict()
    # the other version of the code would be to loop through key_list 
    for i, key in enumerate(key_list):
        if i < val_list_length:
            key_val_dictionary[key] = val_list[i]
        else:
            key_val_dictionary[key] = None
    
    return key_val_dictionary

def strip_single_quote(elem):
    return elem.strip("'")

# main function to accept inputs from the user via command line 
if __name__ == "__main__":
    
    # check if both the values are passed via command line i.e., as arguments
    if len(sys.argv) > 2:
        # the default type is string for arguments we are expecting array based inputs
        # we will clean both the inputs to form the required array 
        entered_key_list = sys.argv[1].lstrip('[').rstrip(']').split(',')    
        entered_val_list = sys.argv[2].lstrip('[').rstrip(']').split(',')
        
        # in case of string passed as element of list we need to strip the single quotes as well
        # when we need to run a function on each of the element using map we get a map object so we need to typecast it to a list
        print(create_dictionary(list(map(strip_single_quote,entered_key_list)),list(map(strip_single_quote,entered_val_list))))
    else:
        # we will take the number of element for the key array 
        entered_key_count = int(input('Enter the number of Key List element : '))
        entered_key_list = []
        # loop through given number of elements
        for i in range(0, entered_key_count):
            # display a nice user friendly message for user to add the element
            print("Enter element No-{}: ".format(i+1))
            elm = input()
            # append it to the list
            entered_key_list.append(elm) 
        # requesting the user to add the value's list now, startign with number of elements in the value list
        entered_val_count = int(input('Enter the number of value List element : '))
        entered_val_list = []
        # looping through the given number of elements in the list
        for i in range(0, entered_val_count):
            print("Enter element No-{}: ".format(i+1))
            # accepting the input
            elm = input()
            # appending it to the value list
            entered_val_list.append(elm) # adding the element
        print(create_dictionary(entered_key_list,entered_val_list))