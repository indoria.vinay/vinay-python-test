import pytest

from question2.solution2 import validate_password_manual,validate_password_regex

def test_validate_password_pass():
    pass_string_arr = ['hello123-d', 'VolAS.-sdq', 'ddsbds-9', 'H']
    for each_string in pass_string_arr:
        is_valid_man = validate_password_manual(each_string)
        is_valid_reg = validate_password_regex(each_string)
        assert is_valid_man == True
        assert is_valid_reg == True

@pytest.mark.xfail
def test_validate_password_fail():
    fail_string_arr = ['1ellfd.-j','Aell\fd.-j','SSJK@123','s897978@-','sdfksbdfsdbsdbfdsbfkdjsdfsfddskdfbsd']
    for each_string in fail_string_arr:
        is_valid_man = validate_password_manual(each_string)
        is_valid_reg = validate_password_regex(each_string)
        assert is_valid_man == False
        assert is_valid_reg == False