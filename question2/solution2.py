import re,sys
import timeit
from timeit import Timer

# manual checkign method: this method is full proof and would work as expected
def validate_password_manual(password):
    # validation for minlength
    if len(password) > 0:
        valid = True
        # validation for first character
        if not password[0].isalpha():
            valid = False 

        # checkign for whether the content match the given rule of alphanumeric, period and hypen(minus)
        if not bool(re.match("^[A-Za-z0-9\\.\\-]+$", password)):
            valid = False
        
        # checking for last character
        if not password[-1].isalnum():
            valid = False
        
        # check for max length
        if len(password) > 20:
            valid = False
        
        return valid
    else:
        raise Exception("Minimum Password length is 1 character")

# a regex based version to the same validation
def validate_password_regex(password):
    # wrote the second condition only to tackle the matching fail during the checking in case of only 1 character string
    # hence was not able to write the entire regex in single call, the idea regex which I first formed was ^[A-Za-z][A-Za-z0-9\.\-]{0,18}[A-Za-z0-9]$
    # tried alot to fix and do it in single regex but there was no point wasting much time. This would impact the time/performance that is for sure
    # return bool(re.match("^[A-Za-z][A-Za-z0-9\.\-]{0,18}[A-Za-z0-9]$", password))
    return (bool(re.match("^[A-Za-z][A-Za-z0-9\\.\\-]{0,19}", password)) and bool(re.match(".*[A-Za-z0-9]$",password)))


if __name__ == "__main__":
    # some text for output
    print('From validate_password_manual :')
    # showing the result
    print(validate_password_manual(sys.argv[1]))
    # timer function to time the execution
    t1 = Timer("validate_password_manual(sys.argv[1])", "from __main__ import validate_password_manual")
    st1 = t1.timeit()
    # printing the millisecond result
    print("validate_password_manual ",st1, "milliseconds")

    # trying out the regex version
    # an interesting fact was when the single regex match was used which I have kept as comment in the code
    # the execution speed of regex was faster than the bruteforce version
    print('From validate_password_regex :')
    # showing the result
    print(validate_password_regex(sys.argv[1]))
    # timer module to log the execution
    t2 = Timer("validate_password_regex(sys.argv[1])", "from __main__ import validate_password_regex")
    st2 = t2.timeit()
    # currently the regex versions is slower as it is doing a comparison 2 times 
    print("validate_password_regex ",st2, "milliseconds")
    
    