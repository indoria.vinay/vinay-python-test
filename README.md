### Python Assignment Submission - Vinay Indoria

Firstly **Thank you** for providing the opportunity to work on the assignment, here is a brief about how can you go ahead and reproduce the same assignment on your machine

### Installation

The requirements.txt file exists in the base directory which can be installed on the preferred virtualenv

```
pip install -r requirements.txt
```

#### Question 1

The question was to get the final output as a dictionary for given key list and value list
For running the tests you can use the command
```
pytest -v question1
```
For running the program directly from shell below are the 2 ways that can be used:
1. passing both the list as shell argument in argv
```
python question1\solution1.py [1,2,3] [4,5,6]
```

2. directly run the solution file to fill in the details for each of the array elements individually
```
python question1\solution1.py
```
The above command will ask series of questions like the total number of elements in key_list, each element in key_list and same questions for value_list

**Note:** Either pass in both parameter or no parameters from the command prompt

#### Question 2
The password validation program, I tried to solve the program by using 2 methods:
- Fully regex based
- Manual checking for given rules

I could have gone ahead and used some library as well but in the end I would have stumbled upon regex to solve corner cases and hence used only 2 approaches

_There was an interesting observation in the program, when I used only a single regex rule to get the desired result - the regex based approach was faster then the manual method. Refer line **35** of the question2\solution2.py file_

For running the test related to the question2
```
pytest -v question2
```
For running the code, we will need the string from the command prompt
```
python question2\solution2.py hello-123
```
Here the text after the file name is the string which needs to be validated, I have used timeit module to check the overall time taken by the code for benchmark purpose. The above command will give the performance information as well.

**Note:** _Upon commenting line 36 and uncommenting line 35 would result in performance imrpovement but unfortunately I was not able to handle a single character string condition in line 35, hence went back to line 36. I would surely crack this but was taking too long._ 

#### Question 3
The output gives the top most IP address along with the count of hits
You can run tests using
```
pytest -v question3
```
For running the program
```
python question3\solution3.py access.log
```
**Note:**_The assumption is made that the file name would be in the same folder as the current path, since the question stated only file name hence made the assumption._
