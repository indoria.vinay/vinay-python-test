import operator, os, sys
import IPy as ip

def get_top_10_ip(filename):
    # get path of the current directory
    __location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
    # open the file
    try:
        file_obj = open(os.path.join(__location__, filename),'r')
    except Exception as exception:
        raise Exception("Given filename does not exist in the current folder")
        
    # initialize an empty dictionary for access during loop through access
    ip_address_count = dict();
    # loop through each line
    for each_access_record in file_obj:
        # we will split via space to get the first item of the array which has IP address 
        each_line_element_arr = each_access_record.split()
        # check if we indeed got something in the line
        if len(each_line_element_arr) > 0:
            # the first element is the IP address as per the given format
            try:
                # if the IP is not valid then ip.IP would generate an exception
                # since the assignment didn't suggested to show relevant IP details of top 10 IP's I have not gone ahead to
                # use Ipy module to get more details of the given IP's, we can easily extend this code to get those details if required
                ip.IP(each_line_element_arr[0])
            except Exception as e:
                continue
            
            current_ip = each_line_element_arr[0]
            # check if the IP already exists in the dictionary
            if current_ip in ip_address_count.keys():
                ip_address_count[current_ip] = ip_address_count[current_ip] + 1
            else:
                ip_address_count[current_ip] = 1
            
    
    file_obj.close()

    # once we have the cooked up dictionary withcoutn assigned we will sort the dictionary in descending order
    return dict( sorted(ip_address_count.items(), key=operator.itemgetter(1),reverse=True)[:10])


if __name__=="__main__":
    # getting the filename from the console
    print(get_top_10_ip(sys.argv[1]))
