import os, pytest

# we will check if the file exists
def test_get_top_10_ip_file_exist():
    filename = 'access.log'
    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    assert os.path.exists(os.path.join(__location__, filename)) == True

@pytest.mark.xfail
def test_get_top_10_ip_file_exist_fail():
    filename = 'doesnotexist.log'
    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    assert os.path.exists(os.path.join(__location__, filename)) == False


def test_get_top_10_ip_file_check_IP_format():
    filename = 'access.log'
    # general current file directory
    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    #file object
    file_obj = open(os.path.join(__location__, filename),'r')
    # loop through each line
    for each_access_record in file_obj:
        # we will split via space to get the first item of the array which has IP address 
        each_line_element_arr = each_access_record.split()
        try:
            # if the IP is not valid then ip.IP would generate an exception
            ip.IP(each_line_element_arr[0])
        except Exception as e:
            # pytest fails the test in-case of exception raised
            print(e)